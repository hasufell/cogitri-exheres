# Copyright 2017 Rasmus Thomsen
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]
require gtk-icon-cache
require github [ user=nextcloud project=client_theming tag=v${PV} ]

SUMMARY="Nextcloud themed desktop client"
DOWNLOADS+=" http://download.owncloud.com/desktop/stable/owncloudclient-${PV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    dolphin [[ description = [ Install the dolphin extension ] ]]
    nautilus [[ description = [ Install the nautilus extension ] ]]
    shibboleth [[ description = [ Support Shibboleth authentication,
        app passwords are a viable alternative ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-db/sqlite:3
        sys-auth/qtkeychain[qt5]
        x11-libs/qtbase:5[gui][sql][sqlite]
        x11-libs/qttools:5
        dolphin? ( kde-frameworks/kio:5
            kde-frameworks/kcoreaddons:5
            kde-frameworks/extra-cmake-modules )
        shibboleth? ( x11-libs/qtwebkit:5 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    test:
        dev-util/cmocka
"

WORK="${WORKBASE}"/owncloudclient-${PV}
CMAKE_SOURCE="${WORKBASE}"/owncloudclient-${PV}

CMAKE_SRC_CONFIGURE_OPTIONS=(
    '!shibboleth NO_SHIBBOLETH'
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_WITH_QT4:BOOL=FALSE
    -DUNIT_TESTING:BOOL=TRUE
    -DDATA_INSTALL_DIR:PATH=/usr/share
    -DSHARE_INSTALL_PREFIX:PATH=/usr/share
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DSYSCONF_INSTALL_DIR:PATH=/etc
    -DOEM_THEME_DIR:PATH="${WORKBASE}"/client_theming-${PV}/nextcloudtheme
)

src_prepare() {
    edo cd "${WORKBASE}"/owncloudclient-${PV}
    if ! optionq nautilus; then
        edo sed -i "/add_subdirectory(nautilus)/d" shell_integration/CMakeLists.txt
    fi

    cmake_src_prepare
}

src_test() {
    export HOME="${TEMP}"
    # required for test # 33 to pass
    unset DISPLAY
    default
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
}

pkg_preinst() {
    gtk-icon-cache_pkg_preinst
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
}



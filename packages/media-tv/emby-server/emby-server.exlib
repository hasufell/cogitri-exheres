# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'emby-server-3.2.27.0.ebuild' from Gentoo, which is:
# Copyright 1999-2017 Gentoo Foundation

export_exlib_phases pkg_setup src_prepare src_compile src_install

require github [ user=MediaBrowser project=Emby tag=${PV} ]
require systemd-service

SUMMARY="Emby Server is a personal media server with apps on just about every device"

LICENCES="GPL-2"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-misc/ca-certificates
        dev-lang/mono[>=4.6.0]
        dev-db/sqlite:3
        dev-dotnet/referenceassemblies-pcl
        group/emby
        media/ffmpeg[vpx]
        user/emby
    recommendation:
        media-gfx/ImageMagick[webp] [[ description = [ required to create thumbnails ] ]]
"

WORK="${WORKBASE}"/Emby-${PV}

# No need to depend on mono repo just for that
export MONO_SHARED_DIR="${TEMP}"
export XDG_CONFIG_HOME="${TEMP}"

emby-server_pkg_setup() {
    exdirectory --allow /opt
}

emby-server_src_prepare() {
    default

    edo cp "${FILES}"/emby-server "${WORK}"
    edo sed -e "s/host/$(exhost --target)/g" -i emby-server
}

emby-server_src_compile() {
    edo xbuild /p:Configuration="Release Mono" /p:Platform="Any CPU" MediaBrowser.sln
}

emby-server_src_install() {
    insinto /opt/emby-server
    doins -r "${WORK}"/MediaBrowser.Server.Mono/bin/Release/*

    keepdir /var/lib/emby-server
    edo chmod g+w "${IMAGE}"/var/lib/emby-server
    edo chown emby:emby "${IMAGE}"/var/lib/emby-server

    exeinto /usr/$(exhost --target)/bin
    doexe emby-server

    install_systemd_files
}


# Copyright Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gogs-0.11.19-r1.exheres-0', which is:
# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Based in part upon 'browserpass-1.0.6.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

SCM_REPOSITORY="git://github.com/dannyvankooten/browserpass.git"
SCM_TAG="${PV}"
SCM_qr_REPOSITORY="git://github.com/rsc/qr.git"
SCM_twofactor_REPOSITORY="git://github.com/gokyle/twofactor.git"
SCM_zglob_REPOSITORY="git://github.com/mattn/go-zglob.git"
SCM_SECONDARY_REPOSITORIES="qr twofactor zglob"

require scm-git

SUMMARY="Chrome & Firefox browser extension for pass, a UNIX password manager"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    build:
        dev-lang/go
    run:
        app-admin/password-store
"

src_prepare() {
    default

    # Point the browser extension to the binary
    HOST_FILE=/usr/$(exhost --target)/bin/browserpass
    ESCAPED_HOST_FILE=${HOST_FILE////\\/}
    edo sed -i -e "s/%%replace%%/${ESCAPED_HOST_FILE}/" chrome/host.json
    edo sed -i -e "s/%%replace%%/${ESCAPED_HOST_FILE}/" firefox/host.json

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/dannyvankooten
    edo mkdir "${WORKBASE}"/build/src/github.com/gokyle
    edo mkdir "${WORKBASE}"/build/src/github.com/mattn
    edo mkdir "${WORKBASE}"/build/src/rsc.io
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/dannyvankooten/browserpass
    edo ln -s "${WORKBASE}"/twofactor "${WORKBASE}"/build/src/github.com/gokyle
    edo ln -s "${WORKBASE}"/zglob "${WORKBASE}"/build/src/github.com/mattn/go-zglob
    edo ln -s "${WORKBASE}"/qr "${WORKBASE}"/build/src/rsc.io
}

src_compile() {
    edo pushd "${WORKBASE}"/build/src/github.com/dannyvankooten/browserpass

    edo go build \
        -o browserpass \
        ./cmd/browserpass

    edo popd
}

src_install() {
    edo pushd "${WORKBASE}"/build/src/github.com/dannyvankooten/browserpass

    dobin browserpass

    insinto /etc/opt/chrome/native-messaging-hosts/
    newins chrome/host.json com.dannyvankooten.browserpass.json
    insinto /etc/opt/chrome/policies/managed/
    newins chrome/policy.json com.dannyvankooten.browserpass.json

    insinto /etc/chromium/native-messaging-hosts/
    newins chrome/host.json com.dannyvankooten.browserpass.json
    insinto /etc/chromium/policies/managed/
    newins chrome/policy.json com.dannyvankooten.browserpass.json

    insinto /usr/$(exhost --target)/lib/mozilla/native-messaging-hosts/
    newins firefox/host.json com.dannyvankooten.browserpass.json

    edo popd
}

pkg_postinst() {
    elog "To use Browserpass, don't forget to install the extention for your browser"
    elog "- https://chrome.google.com/webstore/detail/browserpass/jegbgfamcgeocbfeebacnkociplhmfbk"
    elog "- https://addons.mozilla.org/en-US/firefox/addon/browserpass/"
}

